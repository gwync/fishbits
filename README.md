# Fishbits

Fishbits uses the text editor of your choice to edit encrypted text files. It will use whatever you specify in the EDITOR environment variable. If not specified, it will attempt to use a few that might be present.

Fishbits currently reads and writes AES256. Versions 0.2 and earlier used Blowfish, hence the name.

## Installation

Fishbits requires Python3 and the gnupg module.

For bash completion, copy bash_completion.sh to:

* /etc/bash_completion.d/fishbits

  or

* ~/.local/share/bash-completion/completions/fishbits

---

Fishbits is released under GPLv3 or greater.

---

usage: fishbits [-h] [-v] [-d] ...

Edit and store encrypted files

positional arguments:
  FILE

options:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
  -d, --decrypt  Store file in plain text (default: False)
